module.exports = {
  name: "branciao",
  description: "List status of the elioWay directory structure.",
  run: async ({ branciaos, dir, print }) => {
    let projects = await branciaos()
    let output = new Object()
    for (let group of projects.filter(p => p.elioGroup === "elioway")) {
      output[group.elioName] = projects.filter(p => p.elioGroup === group.elioName).map(p => p.elioName)
    }
    print.info(output)
    print.info({ elioWayPath: dir.elioWayPath })
    print.info({ elioWayRoot: dir.elioWayRoot })
  },
}
