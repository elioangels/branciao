# Installing branciao

- [Prerequisites](./prerequisites)

## npm

Install into your SASS projects.

```
npm install @elioway/branciao
yarn add @elioway/branciao
```

## Contributing

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/elioangels.git
cd elioangels
git clone https://gitlab.com/elioangels/branciao.git
```
